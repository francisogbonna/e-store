﻿using Microsoft.AspNetCore.Mvc;
using SportStore.Stores.Models;
using SportStore.Stores.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Controllers
{
    public class HomeController : Controller
    {
        private IStoreRepository _repository;
        public HomeController(IStoreRepository repo)
        {
            _repository = repo;
        }
        public int PageSize = 4;
        public ViewResult Index(string category,int productPage = 1)
        {
            return View(new ProductListViewModel
            {
                Products = _repository.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductId)
                .Skip((productPage - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = productPage,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? _repository.Products.Count()
                    : _repository.Products.Where(e => e.Category == category).Count()
                },
                CurrentCategory = category
            }); 
        }
    } 
}
