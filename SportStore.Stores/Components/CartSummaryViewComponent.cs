﻿using Microsoft.AspNetCore.Mvc;
using SportStore.Stores.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Components
{
    public class CartSummaryViewComponent : ViewComponent
    {
        private Cart cart { get; set; }
        public CartSummaryViewComponent(Cart cartService)
        {
            cart = cartService;
        }
        public IViewComponentResult Invoke()
        {
            return View(cart);
        }
    }
}
