﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Models
{
    public static  class IdentitySeedData
    {
        private const string adminUser = "Admin";
        private const string adminPassword = "storeAdmin@123";


        public static async void EnsurePopulated(IApplicationBuilder app)
        {
            MyAppDbContext context = app.ApplicationServices.CreateScope().ServiceProvider
                .GetRequiredService<MyAppDbContext>();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            UserManager<IdentityUser> userManager = app.ApplicationServices.CreateScope().ServiceProvider
                .GetRequiredService<UserManager<IdentityUser>>();

            IdentityUser user = await userManager.FindByIdAsync(adminUser);
            if(user == null)
            {
                user = new IdentityUser(adminUser);
                user.Email = "admin@sportsstore.com";
                user.PhoneNumber = "07031620728";

                await userManager.CreateAsync(user, adminPassword);
            }
        }
    }
}
