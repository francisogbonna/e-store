﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Models
{
    public class MyAppDbContext : IdentityDbContext<IdentityUser>
    {
        public MyAppDbContext(DbContextOptions<MyAppDbContext> options): base(options)
        {

        }
    }
}
