﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Models
{
    public class Product
    {
        public long ProductId { get; set; }

        [Required(ErrorMessage ="Product Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Provide Description for Product")]
        public string Description { get; set; }

        [Required]
        [Range(0.01, double.MaxValue,ErrorMessage ="Enter a valid Price for Item")]
        [Column(TypeName="decimal(8,2)")]
        public decimal Price { get; set; }

        [Required(ErrorMessage ="Please Spacify Product Category")]
        public string Category { get; set; }
    }
}
