﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Stores.Models
{
    public class StoreRepository : IStoreRepository
    {
        private StoreDbContext _context;

        public StoreRepository (StoreDbContext dbcontext)
        {
            _context = dbcontext;
        }

        public IQueryable<Product> Products => _context.Products;
        public void CreateProduct(Product p)
        {
            _context.Products.Add(p);
            _context.SaveChanges();
        }
        public void DeleteProduct(Product p)
        {
            _context.Products.Remove(p);
            _context.SaveChanges();
        }
        public void SaveProduct(Product p) => _context.SaveChanges();
    }
}
