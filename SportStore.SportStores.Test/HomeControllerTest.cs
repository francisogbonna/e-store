﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SportStore.Stores.Controllers;
using SportStore.Stores.Models;
using SportStore.Stores.Models.ViewModels;
using Xunit;

namespace SportStore.SportStores.Test
{
    public class HomeControllerTest
    {
        [Fact]
        public void Can_Use_Repository()
        {
            //arrange
            Mock<IStoreRepository> mock = new Mock<IStoreRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product{ProductId = 1, Name ="p1"},
                new Product{ProductId = 2, Name ="p2"}
            }).AsQueryable<Product>());
            HomeController conteoller = new HomeController(mock.Object);

            //act 
            ProductListViewModel result = conteoller.Index(null).ViewData.Model as ProductListViewModel;
            //assert
            Product[] productArray = result.Products.ToArray();
            Assert.True(productArray.Length == 2);
            Assert.Equal("p1", productArray[0].Name);
            Assert.Equal("p2", productArray[1].Name);

        }

        [Fact]
        public void Can_Paginate()
        {
            //arrange
            Mock<IStoreRepository> mock = new Mock<IStoreRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product{ProductId = 1, Name ="p1"},
                new Product{ProductId = 2, Name ="p2"},
                new Product{ProductId = 3, Name ="p3"},
                new Product{ProductId = 4, Name ="p4"},
                new Product{ProductId = 5, Name ="p5"}
            }).AsQueryable<Product>());
            HomeController controller = new HomeController(mock.Object);
            controller.PageSize = 3;

            //act 
            ProductListViewModel result = controller.Index(null,2).ViewData.Model as ProductListViewModel;

            //assert
            Product[] productArray = result.Products.ToArray();
            Assert.True(productArray.Length == 2);
            Assert.Equal("p4", productArray[0].Name);
            Assert.Equal("p5", productArray[1].Name);
        }

        [Fact]
        public void Can_Send_Pagination_ViewModel()
        {
            //arrange
            //Create a mock repository
            Mock<IStoreRepository> mock = new Mock<IStoreRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product{ProductId =1, Name="p1"},
                new Product{ProductId =2, Name="p2"},
                new Product{ProductId =3, Name="p3"},
                new Product{ProductId =4, Name="p4"},
                new Product{ProductId =5, Name="p5"},
                new Product{ProductId =6, Name="p6"}
            }).AsQueryable<Product>());

            HomeController controller = new HomeController(mock.Object) { PageSize = 3 };

            //Act
            ProductListViewModel result = controller.Index(null, 2).ViewData.Model as ProductListViewModel;
            //Asert
            PagingInfo pageInfo = result.PagingInfo;
            Assert.Equal(2, pageInfo.CurrentPage);
            Assert.Equal(3, pageInfo.ItemsPerPage);
            Assert.Equal(5, pageInfo.TotalItems);
            Assert.Equal(2, pageInfo.TotalPages);
        }

        [Fact]
        public void Can_Filter_Product()
        {
            //Arrange 
            //Create a mock repository
            Mock<IStoreRepository> mock = new Mock<IStoreRepository>();
            mock.Setup(p => p.Products).Returns((new Product[]
            {
                new Product{ProductId = 1, Name ="p1", Category ="Cat1"},
                new Product{ProductId = 2, Name ="p2", Category ="Cat2"},
                new Product{ProductId = 3, Name ="p3", Category ="Cat1"},
                new Product{ProductId = 4, Name ="p4", Category ="Cat2"},
                new Product{ProductId = 5, Name ="p5", Category ="Cat3"}
            }).AsQueryable<Product>());

            //Arrange 
            //create a controller and make the page size 3
            HomeController controller = new HomeController(mock.Object);
            controller.PageSize = 3;

            //Action
            Product[] result = (controller.Index("Cat2",1).ViewData.Model as ProductListViewModel).Products.ToArray();

            //Assert
            Assert.Equal(2, result.Length);
            Assert.True(result[0].Name == "p2" && result[0].Category == "Cat2");
            Assert.True(result[1].Name == "p4" && result[1].Category == "Cat2");
        }

        [Fact]
        public void Generate_Category_Specific_Product_Count()
        {
            //Arrange
            Mock<IStoreRepository> mock = new Mock<IStoreRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product{ProductId=1,Name="p1",Category="cat1"},
                new Product{ProductId=2,Name="p2",Category="cat2"},
                new Product{ProductId=3,Name="p3",Category="cat1"},
                new Product{ProductId=4,Name="p4",Category="cat2"},
                new Product{ProductId=5,Name="p5",Category="cat3"}
            }).AsQueryable<Product>());
            HomeController controller = new HomeController(mock.Object);
            controller.PageSize = 3;
            Func<ViewResult, ProductListViewModel> GetModel = result => result?.ViewData?.Model as ProductListViewModel;

            //Action
            int? res1 = GetModel(controller.Index("cat1"))?.PagingInfo.TotalItems;
            int? res2 = GetModel(controller.Index("cat2"))?.PagingInfo.TotalItems;
            int? res3 = GetModel(controller.Index("cat3"))?.PagingInfo.TotalItems;
            int? resAll = GetModel(controller.Index(null))?.PagingInfo.TotalItems;

            //Assert
            Assert.Equal(2, res1);
            Assert.Equal(2, res2);
            Assert.Equal(1, res3);
            Assert.Equal(5, resAll);
        }
    }
}
