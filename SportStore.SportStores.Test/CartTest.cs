﻿using SportStore.Stores.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SportStore.SportStores.Test
{
    public class CartTest
    {
        [Fact]
        public void Can_Add_New_Lines()
        {
            //Arrange Creates test products
            Product p1 = new Product { ProductId = 1, Name = "p1" };
            Product p2 = new Product { ProductId = 2, Name = "p2" };
            //Arrange - creates a new cart
            Cart target = new Cart();

            //Act
            target.AddItems(p1, 1);
            target.AddItems(p2, 1);
            CartLine[] result = target.Lines.ToArray();

            //Assert 
            Assert.Equal(2, result.Length);
            Assert.Equal(p1, result[0].Product);
            Assert.Equal(p2, result[1].Product);
        }

        [Fact]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //Arrange Creates test products
            Product p1 = new Product { ProductId = 1, Name = "p1" };
            Product p2 = new Product { ProductId = 2, Name = "p2" };
            //Arrange - creates a new cart
            Cart target = new Cart();

            //Act
            target.AddItems(p1, 1);
            target.AddItems(p2, 1);
            target.AddItems(p1, 10);
            CartLine[] results = target.Lines.OrderBy(c => c.Product.ProductId).ToArray();

            //assert
            Assert.Equal(2, results.Length);
            Assert.Equal(11, results[0].Quantity);
            Assert.Equal(1, results[1].Quantity);
        }

        [Fact]
        public void Can_Remove_Line()
        {
            //Arrange
            Product p1 = new Product { ProductId = 1, Name = "p1" };
            Product p2 = new Product { ProductId = 2, Name = "p2" };
            Product p3 = new Product { ProductId = 3, Name = "p3" };
            //Create a new Cart
            Cart target = new Cart();
            //add item to cart
            target.AddItems(p1, 1);
            target.AddItems(p2, 3);
            target.AddItems(p3, 5);
            target.AddItems(p2, 1);

            //Act 
            target.RemoveLine(p2);

            //assert
             Assert.Empty(target.Lines.Where(c => c.Product == p2));
            Assert.Equal(2, target.Lines.Count());
        }

        [Fact]
        public void Calculate_Cart_Total()
        {
            //arrange - create test products
            Product p1 = new Product { ProductId = 1, Name = "p1", Price = 100M };
            Product p2 = new Product { ProductId = 2, Name = "p2", Price = 50M };
            Cart target = new Cart();

            //Act 
            target.AddItems(p1, 1);
            target.AddItems(p2, 1);
            target.AddItems(p1, 3);
            decimal results = target.ComputTotalValue();

            //Assert
            Assert.Equal(450M, results);
        }

        [Fact]
        public void Can_Clear_Content()
        {
            //Arrange - create test products
            Product p1 = new Product { ProductId = 1, Name = "p1", Price = 100M };
            Product p2 = new Product { ProductId = 2, Name = "p2", Price = 50M };
            Cart target = new Cart();

            //Act
            target.AddItems(p1, 1);
            target.AddItems(p2, 1);
            target.Clear();

            //assert
            Assert.Empty(target.Lines);
        }
    }
}
