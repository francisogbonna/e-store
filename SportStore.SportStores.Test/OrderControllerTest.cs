﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using SportStore.Stores.Controllers;
using SportStore.Stores.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SportStore.SportStores.Test
{
    public class OrderControllerTest
    {
        [Fact]
        public void Cannot_Checkout_Empt_Cart()
        {
            //Arrange - create mock repository
            Mock<IOrderRepository> mockRepository = new Mock<IOrderRepository>();
            //Create an empty cart 
            Cart cart = new Cart();
            //create order 
            Order order = new Order();
            //create controller instance
            OrderController target = new OrderController(mockRepository.Object, cart);

            //Act
            ViewResult result = target.Checkout(order) as ViewResult;

            //check that order hasn't been stored
            mockRepository.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Never);
            //Check that the method is returning the default view 
            Assert.True(String.IsNullOrEmpty(result.ViewName));
            //Check that valid model is passed
            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Fact]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            //Arrange - create a mock repository
            Mock<IOrderRepository> mockRepo = new Mock<IOrderRepository>();
            //create cart with one item
            Cart cart = new Cart();
            cart.AddItems(new Product(), 1);
            //create an instance of the Controller
            OrderController target = new OrderController(mockRepo.Object, cart);
            //add error to the modelState
            target.ModelState.AddModelError("Error", "Error");

            //Act - try checking out
            ViewResult result = target.Checkout(new Order()) as ViewResult;

            //Assert 
            mockRepo.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Never);
            //Check its returning the default view
            Assert.True(String.IsNullOrEmpty(result.ViewName));
            //Check model state is invalid
            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Fact]
        public void Can_Checkout_And_Submit_Order()
        {
            //Arrange - create a mock repository
            Mock<IOrderRepository> mockRepo = new Mock<IOrderRepository>();
            Cart cart = new Cart();
            cart.AddItems(new Product(), 1);
            OrderController target = new OrderController(mockRepo.Object, cart);

            //Act
            RedirectToPageResult result = target.Checkout(new Order()) as RedirectToPageResult;

            //Assert
            //Check that order has been stored
            mockRepo.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Once);
            //Checks if order is submitted
            Assert.Equal("/completed", result.PageName);
        }
    }
}
